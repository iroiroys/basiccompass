package com.tistory.oysu.basiccompass

import android.widget.SeekBar

// for test the UI
class OnSeekbarChangeListenerImpl(private val cb: SeekBarToView) : SeekBar.OnSeekBarChangeListener{

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        cb.onValue(progress.toFloat())
    }
}

// to View
interface SeekBarToView {
    fun onValue(degree: Float)
}