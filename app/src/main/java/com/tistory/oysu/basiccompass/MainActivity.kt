package com.tistory.oysu.basiccompass

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SeekBarToView, SensorToView{

    private val sensorImpl = SensorEventListenerImpl(this)
    private lateinit var sm: SensorManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // for test ui
        sb.setOnSeekBarChangeListener(OnSeekbarChangeListenerImpl(this))
        // ~~

        sm = getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    override fun onResume() {
        super.onResume()
        sm.registerListener(sensorImpl, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
        sm.registerListener(sensorImpl, sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sm.unregisterListener(sensorImpl)
    }

    // from Listener
    override fun onValue(degree: Float) {
        mainCanvas.degree = degree
    }

    // from Listener
    override fun onDegreeChanged(degree: Float) {
        mainCanvas.degree = degree
    }
}
