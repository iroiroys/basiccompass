package com.tistory.oysu.basiccompass

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager


@Suppress("LocalVariableName")
class SensorEventListenerImpl(private val cb: SensorToView) : SensorEventListener {

    private var gravity: FloatArray? = null
    private var geomagnetic: FloatArray? = null

    override fun onSensorChanged(event: SensorEvent?) {
        event?.let {
            if (event.sensor.type == Sensor.TYPE_ACCELEROMETER)
                gravity = event.values
            if (event.sensor.type == Sensor.TYPE_MAGNETIC_FIELD)
                geomagnetic = event.values
            if (gravity != null && geomagnetic != null) {
                val R = FloatArray(9)
                val I = FloatArray(9)
                if (SensorManager.getRotationMatrix(R, I, gravity, geomagnetic)) {
                    val orientation = FloatArray(3)
                    SensorManager.getOrientation(R, orientation)
                    cb.onDegreeChanged(Math.toDegrees(orientation[0].toDouble()).toFloat())
                }
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }
}


// to View
interface SensorToView {
    fun onDegreeChanged(degree: Float)
}