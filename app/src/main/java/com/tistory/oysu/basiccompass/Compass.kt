package com.tistory.oysu.basiccompass

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

private const val FIRST_RING_WIDTH = 10f

class Compass(ctx: Context, attr: AttributeSet?) : View(ctx, attr) {

    var degree: Float = 0.0f
        set(value) {
            // 90˚ offset
            field = value + 90f
            invalidate()
        }

    constructor(ctx: Context) : this(ctx, null)

    private val blackCirclePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val whiteCirclePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val indicatorPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)


    init {
        // Paint for black outer circle
        blackCirclePaint.color = Color.BLACK

        // Paint for white inner circle
        whiteCirclePaint.color = Color.WHITE

        // line indicator
        indicatorPaint.strokeWidth = 5f
        indicatorPaint.strokeCap = Paint.Cap.ROUND
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            myDraw(it)
        }
    }

    private fun myDraw(canvas: Canvas) {

        val halfHeight = canvas.height / 2f
        val radius = canvas.width / 2f
        canvas.drawCircle(radius, halfHeight, radius, blackCirclePaint)
        canvas.drawCircle(radius, halfHeight, radius - FIRST_RING_WIDTH, whiteCirclePaint)

        canvas.drawLine(radius, halfHeight, radius + calculateXWithDegree(radius, degree), halfHeight + calculateYWithDegree(radius, degree), indicatorPaint)
    }

    private fun calculateXWithDegree(r: Float, degree: Float): Float {
        // a/r = cosθ
        return r * Math.cos(Math.toRadians(degree.toDouble())).toFloat()
    }

    private fun calculateYWithDegree(r: Float, degree: Float): Float {
        // b/r = sinθ
        return r * Math.sin(Math.toRadians(degree.toDouble())).toFloat()
    }
}